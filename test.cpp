///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file test.cpp
/// @version 1.0
///
/// Unit tests for Cat and CatEmpire functions. 
///
/// @author Marie Wong <marie4@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   20_APR_2020
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <cassert>

#include "cat.hpp"

using namespace std;

const int NUMBER_OF_CATS = 20;

int main() {
	cout << "Welcome to Cat Empire!" << endl;
   cout << "TEST VERSION" << endl;

	Cat::initNames();

   //initalize a cat and empire
  // Cat* cat = Cat::makeCat();
	CatEmpire catEmpire;

   assert( catEmpire.empty() );
   assert( catEmpire.size() == 0 );

	for( unsigned int i = 0 ; i < NUMBER_OF_CATS ; i++ ) {
		Cat* newCat = Cat::makeCat();

      assert( catEmpire.size() == i );
		
      catEmpire.addCat( newCat );
	   
      assert( catEmpire.size() == ( i + 1 ) );
   }
   
   assert( catEmpire.size() == NUMBER_OF_CATS );
   assert( catEmpire.validate() );

   catEmpire.dump();

//   cout << "Print a family tree of " << NUMBER_OF_CATS << " cats" << endl;

//   catEmpire.catFamilyTree()
      ;
   

//   cout << "Print an alphabetized list of " << NUMBER_OF_CATS << " cats" << endl;

//   catEmpire.catList();

//	cout << "Print a pedigree of " << NUMBER_OF_CATS << " cats" << endl;

//   catEmpire.catBegat();

   cout << "List of " << NUMBER_OF_CATS << " cats by generation" << endl;

   catEmpire.catGenerations();


} // main()
