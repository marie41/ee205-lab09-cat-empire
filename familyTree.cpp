///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file familyTree.cpp
/// @version 1.0
///
/// Print a family tree of cats
///
/// @author Marie Wong <marie4@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   27_APR_2020
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "cat.hpp"

using namespace std;

const int NUMBER_OF_CATS = 20;

int main() {
	cout << "Welcome to Cat Empire!" << endl;

	Cat::initNames();

	CatEmpire catEmpire;

	for( int i = 0 ; i < NUMBER_OF_CATS ; i++ ) {
		Cat* newCat = Cat::makeCat();
      
     // cout << "Name=[" << newCat->name << "] size=[" << newCat->name.size() << "]" << endl;
		
      catEmpire.addCat( newCat );
	}

	cout << "Print a family tree of " << NUMBER_OF_CATS << " cats" << endl;

	catEmpire.catFamilyTree();

} // main()
