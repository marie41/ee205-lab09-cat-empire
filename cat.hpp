///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.hpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Marie Wong <marie4@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   20_APR_2021 
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <queue>

class Cat {
private:    /// Member variables
	std::string name;

public:     /// Constructors
	Cat( const std::string newName );
	friend class CatEmpire;

private:    /// Private methods
	void setName( const std::string newName ); // Name is a key in our BST, so we don't
		                                        // want to let people change it as it'll
		                                        // mess up the tree.

private:    /// Static variables
	static std::vector<std::string> names;

public:     /// Static methods
	static void initNames();
	static Cat* makeCat();
   
public:
   //prints Cat link data
   void dump() const;

protected:  //Storage variables
   Cat* left = nullptr;
   Cat* right = nullptr;

};//end of Cat class


class CatEmpire {
private:
	Cat* topCat = nullptr;

protected:
   unsigned int  count = 0;

public:
	const bool empty() const;  // Return true if empty
   inline unsigned int size() const { return count; };
public:
	void addCat( Cat* newCat );  // Add a cat starting at the root
	
   void catFamilyTree() const;
	void catList() const;
	void catBegat() const;
	void catGenerations();

   void dump() const;   //recursively prints empire
   bool validate() const;  //validates Empire
   const bool isIn( Cat* aCat ) const;    //check if cat is in empire

private:
	void addCat( Cat* atCat, Cat* newCat );  // Add a cat starting at atCat

	void dfsInorderReverse( Cat* atCat, int depth ) const;
	void dfsInorder( Cat* atCat ) const;
	void dfsPreorder( Cat* atCat ) const;

   void bfs( Cat* atCat, std::vector< std::queue< Cat* > >* tree, int gen );
   void getEnglishSuffix( int n ) const; //prints number n with appropriate suffix

};//end of CatEmpire class
