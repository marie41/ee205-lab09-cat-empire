///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Marie Wong <marie4@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   27_APR_2021
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>
#include <queue>
#include <string>
#include <vector>

#define CAT_NAMES_FILE "names.txt"

#include "cat.hpp"

using namespace std;

Cat::Cat( const std::string newName ) {
	setName( newName );
}


void Cat::setName( const std::string newName ) {
	assert( newName.length() > 0 );

	name = newName;
}


std::vector<std::string> Cat::names;


void Cat::initNames() {
	names.clear();
	cout << "Loading... ";

	ifstream file( CAT_NAMES_FILE );
	string line;
	while (getline(file, line)) names.push_back(line);

	cout << to_string( names.size() ) << " names." << endl;
}


Cat* Cat::makeCat() {
	// Get a random cat from names
	auto nameIndex = rand() % names.size();
	auto name = names[nameIndex];

	// Remove the cat (by index) from names
	erase( names, names[nameIndex] );

	return new Cat( name );
}

//add cat at root.
void CatEmpire:: addCat( Cat* newCat ) {
   if ( topCat != nullptr ) {
//      cout << "Tree is not empty, adding cat at next leaf" << endl;
      addCat( topCat, newCat );

   } else {
      topCat = newCat;
      count++;
   }
}

//add cat at atCat
void CatEmpire::addCat( Cat* atCat, Cat* newCat ) {
 
  // cout << "Name=[" << newCat->name << "] size=[" << newCat->name.size() << "]" << endl;
  
   if( topCat == nullptr ) { addCat( newCat ); } //newCat is root
   
   if( atCat->name > newCat->name ) {
      if( atCat->left == nullptr ) { // leaf is empty
         atCat->left = newCat;
         count++;

      } else {
         addCat( atCat->left, newCat ); //check next leaf
      }
   } else { //atCat->name < newCat->name
      if(atCat->right == nullptr ) {
         atCat->right = newCat;
         count++;

      } else {
         addCat( atCat->right, newCat );
      }
   }
   
   //cout << "Name=[" << newCat->name << "] size=[" << newCat->name.size() << "]" << endl;

}


void CatEmpire::catFamilyTree() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsInorderReverse( topCat, 1 );
}

//right,parent,left
void CatEmpire::dfsInorderReverse( Cat* atCat, int depth ) const {
//   cout << "Name=[" << atCat->name << "] size=[" << atCat->name.size() << "]";
//   cout << "   Depth=[" << depth << "]" << endl; 
//   atCat->dump();

   if( atCat == nullptr ) return;

   //all the way down right
   if ( atCat->right != nullptr ) {
      dfsInorderReverse( atCat->right, (depth + 1) );

   }

   //print current node
   cout << string( 6 * (depth-1), ' ' ) << atCat->name;
  // cout << depth;

   if( atCat->right == nullptr && atCat->left == nullptr ) {
      cout << endl;
      return;
   }
   
   if( atCat->right != nullptr && atCat->left != nullptr ) {
      cout << "<" << endl;
   }
   
   if( atCat->right == nullptr && atCat->left != nullptr ) {
      cout << "\\" << endl;
   }

   if( atCat->right != nullptr && atCat->left == nullptr ) {
      cout << "/" << endl;
   }


   if( atCat->left != nullptr ) {
      dfsInorderReverse( atCat->left, (depth + 1) );
   }

}



void CatEmpire::catList() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsInorder( topCat );
}


//left, parent, right
void CatEmpire::dfsInorder( Cat* atCat ) const {
   if( atCat == nullptr ) return;

   dfsInorder( atCat->left );
   cout << atCat->name << endl;
   dfsInorder( atCat->right );
}




void CatEmpire::catBegat() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsPreorder( topCat );
}

//parent, left, right
void CatEmpire::dfsPreorder( Cat* atCat ) const {
   if( atCat == nullptr ) return;

   if( atCat->left != nullptr || atCat->right != nullptr ) {
      cout << atCat->name << " begat ";
      
      if( atCat->left != nullptr ) {
         cout << atCat->left->name;

         if( atCat->right != nullptr ) {
            cout << " and ";
         }
      }

      if( atCat->right != nullptr ) {
         cout << atCat->right->name;
      }
      cout << endl;
   }

   dfsPreorder( atCat->left );
   dfsPreorder( atCat->right );
}


void CatEmpire::catGenerations() {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

   //initialize vector of strings then add topCat as member of first generation
   vector< queue< Cat* > > tree(1);
   tree[0].push( topCat );

	bfs( topCat, &tree, 1 );
   
   ////vector< queue< Cat* > > tree (generations);
   //iterate through tree vector
   for( int i = 0 ; i < (int)tree.size() ; i++ ) {
      
      if( tree[i].empty() ) { break; }
      //print individual generation queues
      getEnglishSuffix(i + 1);
      cout << " Generation" << endl;
      
      while ( !tree[i].empty() ) {
         cout << "  " << tree[i].front()->name;
         tree[i].pop();
      }
      cout << endl;
   }
}

//prints tree in bfs form
void CatEmpire::bfs( Cat* rootCat, vector< queue< Cat* > >* tree, int gen ) {
   if( rootCat == nullptr ) return;

   //add a new queue to tree vector if no existing queue for this generation
  
   //cout << "hey" << endl;
   //cout << "gen=[" << gen << "]  size=[" << tree->size() << "]" << endl;
   if( tree->size() <= (unsigned int)gen ) {
      //cout << " tree[" << gen << "] is empty." << endl;
      queue< Cat* > cats;
      tree->push_back( cats );
   }
   //cout << "ho" << endl;
   if( rootCat->left != nullptr ) {
      tree->at(gen).push( rootCat->left );      
   }
   //cout << "let's go" << endl;
   if( rootCat->right != nullptr ) {
      tree->at(gen).push( rootCat->right );
   }
   
   gen++;

   bfs( rootCat->left, tree, gen );
   bfs( rootCat->right, tree, gen );
   //cout << "queue size=[" << tree[gen].size() << "]" << endl;
   //getEnglishSuffix( 3 );

}

//prints n with appropriate suffix, supports catGeneration bfs
void CatEmpire::getEnglishSuffix( int n ) const {
   
   int c = 0;
   string n_suf = "";

   //0-19
   if( n > 10 && n < 14 ) {
      c = 0;
   } else {
      c =  n % 10;
   }
      
   switch( c ) {
      case 0: n_suf = "th";
              break;
      case 1: n_suf = "st";
              break;
      case 2: n_suf = "nd";
              break;
      case 3: n_suf = "rd";
              break;
      default: n_suf = "th";
   }

   cout << n << n_suf;
}


void Cat::dump() const {
   cout << "Cat:  Cat->left=[" << left << "] Cat=[" << this << "] Cat->right=[" << right << "]" << endl;
}

//recursively prints Empire data using DF preorder traversal
void CatEmpire::dump() const {
   if ( topCat == nullptr ) {
      cout << "Empire has been ransacked. No cats were spared." << endl;
      return;
   }

   cout << "CatEmpire:  topCat=[" << topCat << "]" << endl;
   Cat* currentCat = topCat;

   for (  ; currentCat != nullptr ; currentCat = currentCat->left ) {
      cout << "   ";
      currentCat->dump();
   }

   for ( Cat* currentCat_R = currentCat ; currentCat_R != nullptr ; currentCat_R = currentCat_R->right ) {
      cout << "   ";
      currentCat_R->dump();
   }
   
   for ( Cat* currentCat_L = currentCat ; currentCat_L != nullptr ; currentCat_L = currentCat_L->left ) {
      cout << "   ";
      currentCat_L->dump();
   }
}

bool CatEmpire::validate() const {
    //if topCat is nullptr, list is empty, and  size is 0
   if ( topCat == nullptr ) {
      assert( empty() );
      assert( count == 0 );
   //if topCat is not nullptr, list is not empty, size is greater than 0
   } else {
      assert( !empty() );
      assert( count > 0 );
      assert( topCat->left != nullptr || topCat->right != nullptr );
   }

   //if there's one cat, it is topCat, and topCat->left/right are nullptr
   if ( count == 1 ) {
      assert( !empty() );
      assert( topCat->left == nullptr && topCat->right == nullptr );
   }

   return true;//passes all tests
}

const bool CatEmpire::empty() const {
   if ( topCat == nullptr ) return true;

   return false;
}

const bool CatEmpire::isIn( Cat* aCat ) const {
   if ( topCat == nullptr ) return false;

   Cat* currentCat = topCat;

   for (  ; currentCat != nullptr ; currentCat = currentCat->left ) {
      if ( currentCat == aCat ) return true;
      currentCat->dump();
   }

   for ( Cat* currentCat_R = currentCat ; currentCat_R != nullptr ; currentCat_R = currentCat_R->right ) {
      if ( currentCat_R == aCat ) return true;
      currentCat_R->dump();
   }
   
   return false;
}



///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
